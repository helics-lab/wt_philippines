import argparse
from pathlib import Path

import pandas as pd
import os
import csv
from cryptography.fernet import Fernet
from tqdm import tqdm

tqdm.pandas()
csv.field_size_limit(13107200)

parser = argparse.ArgumentParser()
parser.add_argument('--process_annotated_files', '-annot', dest='process_annotated_files', type=lambda s: s.lower() in ['true', 't', 'yes', '1'],
                    help='Process annotated files. This argument is required.', required=True)
args = parser.parse_args()

process_annotated_files = args.process_annotated_files
subsample_rows = 0  # 0 for all rows


if process_annotated_files:
    path = 'output/filtered' + os.sep
    data_fn = path + 'Tiktok_filtered.json'
else:
    path = 'output' + os.sep
    data_fn = path + 'Tiktok_cleaned.json'


def encrypt(message: bytes, key: bytes) -> bytes:
    return Fernet(key).encrypt(message)


def decrypt(token: bytes, key: bytes) -> bytes:
    return Fernet(key).decrypt(token)


def generate_encryption_key():
    key = Fernet.generate_key()
    print("Key:", key.decode())
    with open('KEY_DB.txt', 'w') as file:
        file.write(key.decode())


print('Reading data input...')
df_all = pd.read_json(data_fn, lines=True)

if subsample_rows > 0:
    df = df_all.head(subsample_rows)
else:
    df = df_all
df.reset_index(drop=True, inplace=True)

# Add 'common_name', 'scientific_name' columns
df_names = pd.read_json('input/common_names.json', lines=True)


def common_name_preproc(row):
    return df_names.loc[row.id-1, 'common_name']


def scientific_name_preproc(row):
    return df_names.loc[row.id-1, 'scientific_name']


if 'common_name' not in df.columns:
    df.insert(1, 'common_name', "")
    df.insert(2, 'scientific_name', "")
    print('Adding common and scientific name columns...')
    df.common_name = df.apply(common_name_preproc, axis=1)
    df.scientific_name = df.apply(scientific_name_preproc, axis=1)

if not os.path.isfile('KEY_DB.txt'):
    generate_encryption_key()

try:
    with open('KEY_DB.txt', 'r') as file:
        key = file.read().encode()
except FileNotFoundError:
    print("You should create a file <KEY_DB.txt> contains the encryption key. "
          "The key should be 32 url-safe base64-encoded bytes. "
          "Please run the function generate_encryption_key() at least once. ")

df_encrypted = df
cols_to_encrypt = ['tiktok_id', 'author_id', 'author_uniqueId', 'author_nickname', 'author_signature', 'video_id',
                   'video_cover_url', 'video_origin_cover_url', 'tiktok_url']

# For faster encryption, columns will be converted to string type
print('Converting column data that needs encryption into Str...')
for col in tqdm(cols_to_encrypt):
    df_encrypted[col] = df_encrypted[col].apply(lambda x: str(x))

print()
print('Encrypting sensitive columns...')


def encrypt_df_row(row):
    for col in cols_to_encrypt:
        data = row[col]
        row[col] = encrypt(data.encode(), key).decode()
    return row


def decrypt_df_row(row):
    for col in cols_to_encrypt:
        data = row[col]
        row[col] = decrypt(data.encode(), key).decode()
    return row


# use `progress_apply` instead of `apply`
df_encrypted = df_encrypted.progress_apply(lambda x: encrypt_df_row(x), axis=1)

print()
print('Finished encrypting all sensitive columns...')

# Replace spaces in column names into underscore
columns = df_encrypted.columns
new_columns = []
for col in columns:
    new_columns.append(col.replace(" ", "_"))
df_encrypted.columns = new_columns

path_save = Path().absolute().parent / 'output_encrypted/tiktok'
fname = path_save / "Tiktok"

print()
print('Saving data...')
df_encrypted.to_json(fname.with_suffix('.json'), orient='records', lines=True, force_ascii=False)

# Saving the csv with special character separator like ζ works well with even non-english commonly used text
# Replace newlines to not affect importing them in Spreadsheets
df_encrypted = df_encrypted.applymap(lambda x: str(x).replace('\n', ' '))
df_encrypted = df_encrypted.applymap(lambda x: str(x).replace('\r', ' '))
df_encrypted.to_csv(fname.with_suffix('.csv'), encoding='utf-8-sig', sep='ζ', index=False)

print(f"Tiktok data file saved to {fname}")
