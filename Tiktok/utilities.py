import os

# print with colors
# https://stackoverflow.com/a/287944/1970830
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# If filename exists add number to the file name to prevent overwrite
# Any file name will be modified to add '_v'. If this is the first file '_v1' will be added
# modified version of:
# https://stackoverflow.com/a/29019430/1970830
def build_filename(name, num=1):  # if num = 0 '_v?' will not be added
    root, ext = os.path.splitext(name)
    return '%s%s%d%s' % (root, '_v', num, ext) if num else name

def find_next_filename(name, max_tries=1000):
    for i in range(1, max_tries):
        test_name = build_filename(name, i)
        if not os.path.exists(test_name): return test_name
    return None
