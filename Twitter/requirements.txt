twint==2.1.22
cryptography==3.4.8
pandas==1.3.3
reverse_geocode==1.4.1
tqdm==4.62.2
