import pandas as pd
import os
import glob

path = f'output{os.sep}'
combined = f'output_merged{os.sep}data_all'

folders = glob.glob(path + '*')

mx = 0
for item in folders:
    nm = int(item.split('-')[0].split(os.sep)[1])
    if nm > mx: mx = nm

if not os.path.exists('output_merged'):
    os.makedirs('output_merged')

dfs = []
for i in range(1, mx+1):
    print()
    print(f'Searching for species id={i}')
    sp_names = glob.glob(f'{path}{i}-*')
    for nm in sp_names:
        print(f'Reading data of {nm}')
        df = pd.read_json(nm+os.sep+"tweets.json", lines = True)
        df.columns = ['twitter_id' if cl == 'id' else cl for cl in df.columns]
        df.insert(0, 'search_term', f"{nm.split(os.sep)[1].split('-', 1)[1]}")
        df.insert(0, 'id', f"{i}")
        dfs.append(df)
print()
print('Merging all species data into a single file.. This will take a few minutes..')
df_all = pd.concat(dfs, axis=0)
df_all.reset_index(drop=True, inplace=True)

# Save it as json
data_fn = combined + '.json'
df_all.to_json(data_fn, orient='records', lines=True, force_ascii=False)

# Save it as csv for inspection
df_all = df_all.applymap(
    lambda x: str(x).replace('\n', ' '))  # Replace newlines to not affect importing them in Spreadsheets
df_all = df_all.applymap(lambda x: str(x).replace('\r', ' '))

data_fn = combined + '.csv'
df_all.to_csv(data_fn, encoding='utf-8-sig', sep='ζ', index=False)






