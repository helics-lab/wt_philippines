import pandas as pd
import os
import csv
from cryptography.fernet import Fernet
from tqdm import tqdm
import argparse
from pathlib import Path

tqdm.pandas()
csv.field_size_limit(13107200)

parser = argparse.ArgumentParser()
parser.add_argument('--affix', '-a', dest='affix', type=str,
                    help='affix for saving filenames and data. Example if Philippines restricted area is needed then affix can be ph or PH. This argument is required.', required=True)
parser.add_argument('--process_annotated_files', '-annot', dest='process_annotated_files', type=lambda s: s.lower() in ['true', 't', 'yes', '1'],
                    help='Process annotated files. This argument is required.', required=True)
args = parser.parse_args()

affix = args.affix
process_annotated_files = args.process_annotated_files
subsample_rows = 0  # 0 for all rows

if process_annotated_files:
    path = Path(f'output{os.sep}{affix.upper()}_with_additions{os.sep}filtered')
    data_fn = path / f'Google_{affix.lower()}_filtered.json'
else:
    path = Path(f'output{os.sep}{affix.upper()}_with_additions')
    data_fn = path / f'Google_{affix.lower()}_cleaned.json'


def encrypt(message: bytes, key: bytes) -> bytes:
    return Fernet(key).encrypt(message)


def decrypt(token: bytes, key: bytes) -> bytes:
    return Fernet(key).decrypt(token)


def generate_encryption_key():
    key = Fernet.generate_key()
    print("Key:", key.decode())
    with open('KEY_DB.txt', 'w') as file:
        file.write(key.decode())


print('Reading data input...')
df_all = pd.read_json(data_fn, lines=True)

if subsample_rows > 0:
    df = df_all.head(subsample_rows)
else:
    df = df_all

df = df.drop(['total results count', 'NewsArticle_dateCreated', 'NewsArticle_itemtype', 'WebPage_inLanguage',
              'metatags_medium', 'metatags_article_publisher', 'metatags_site_name', 'NewsArticle_inLanguage',
              'metatags_news_keyword', 'metatags_pubdate', 'metatags_lastmod', 'metatags_article_tags'
              ], axis=1)
df.reset_index(drop=True, inplace=True)

# Add 'common_name', 'scientific_name' columns
df_names = pd.read_json(f'input{os.sep}common_names.json', lines=True)


def common_name_preproc(row):
    return df_names.loc[row.id-1, 'common_name']


def scientific_name_preproc(row):
    return df_names.loc[row.id-1, 'scientific_name']


if 'common_name' not in df.columns:
    df.insert(1, 'common_name', "")
    df.insert(2, 'scientific_name', "")
    print('Adding common and scientific name columns...')
    df.common_name = df.apply(common_name_preproc, axis=1)
    df.scientific_name = df.apply(scientific_name_preproc, axis=1)


if not os.path.isfile('KEY_DB.txt'):
    generate_encryption_key()

try:
    with open('KEY_DB.txt', 'r') as file:
        key = file.read().encode()
except FileNotFoundError:
    print("You should create a file <KEY_DB.txt> contains the encryption key. "
          "The key should be 32 url-safe base64-encoded bytes. "
          "Please run the function generate_encryption_key() at least once. ")

# Encrypt columns
df_encrypted = df.copy()
cols_to_encrypt = ['link', 'person_dict',
                   'organization_dict', 'metatags_twitter_site', 'metatags_author']

# For faster encryption, columns will be converted to string type
print()
print('Converting column data that needs encryption into Str...')
for col in tqdm(cols_to_encrypt):
    df_encrypted[col] = df_encrypted[col].apply(lambda x: str(x))


def encrypt_df_row(row):
    for col in cols_to_encrypt:
        data = row[col]
        # if not isinstance(data, str):
        #     data = str(data)
        row[col] = encrypt(data.encode(), key).decode()
    return row


def decrypt_df_row(row):
    for col in cols_to_encrypt:
        data = row[col]
        # if not isinstance(data, str):
        #     data = str(data)
        row[col] = decrypt(data.encode(), key).decode()
    return row


print()
print('Encrypting sensitive columns...')
df_encrypted = df_encrypted.progress_apply(lambda x: encrypt_df_row(x), axis=1)

print()
print('Finished encrypting all sensitive columns...')

# Replace spaces in column names into underscore
columns = df_encrypted.columns
new_columns = []
for col in columns:
    new_columns.append(col.replace(" ", "_"))
df_encrypted.columns = new_columns


path_save = Path().absolute().parent / f'output_encrypted{os.sep}google'
fname = path_save / f"Google_{affix.lower()}"

print('Saving data...')
df_encrypted.to_json(fname.with_suffix('.json') , orient='records', lines=True, force_ascii=False)

# Saving the csv with special character separator like ζ works well with even non-english commonly used text
# Replace newlines to not affect importing them in Spreadsheets
df_encrypted = df_encrypted.applymap(lambda x: str(x).replace('\n', ' '))
df_encrypted = df_encrypted.applymap(lambda x: str(x).replace('\r', ' '))
df_encrypted.to_csv(fname.with_suffix('.csv'), encoding='utf-8-sig', sep='ζ', index=False)

print(f"Google data file saved to {fname}")
