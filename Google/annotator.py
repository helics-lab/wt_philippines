import os

import pandas as pd
import csv
from tqdm import tqdm
from pathlib import Path
import argparse

tqdm.pandas()
csv.field_size_limit(13107200)

parser = argparse.ArgumentParser()
parser.add_argument('--affix', '-a', dest='affix', type=str,
                    help='affix for saving filenames and data. Example if Philippines restricted area is needed then affix can be ph or PH. This argument is required.', required=True)
parser.add_argument('--annotate_encrypted_files', '-enc', dest='annotate_encrypted_files', type=lambda s: s.lower() in ['true', 't', 'yes', '1'],
                    help='Annotate the encrypted files. This argument is required.', required=True)
args = parser.parse_args()

affix = args.affix
annotate_encrypted_files = args.annotate_encrypted_files
subsample_rows = 0  # 0 for all rows

# Some filter terms intentionally preceded with space to avoid words that matches partially with them.
relevance_filter_terms = [' sell', ' sale', ' trade', 'usd', 'peso', ' cost', ' price',
'alibaba', 'reddit', 'amazon', 'lazada', 'shopee', 'carousell', 'ebay',
' ibenta', ' pagbebenta', ' kalakal', ' gastos', ' presyo']

columns_to_search = ['title', 'snippet', 'article_articlebody', 'metatags_article_tag', 'metatags_image_alt',
                     'metatags_description', 'metatags_twitter_description', 'NewsArticle_description',
                     'scraped_webpage']  # 'scraped_webpage' is not column but will be used to read scraped txt files

if annotate_encrypted_files:
    path = Path().absolute().parent / f'output_encrypted{os.sep}google'
    data_fn = path / f'Google_{affix.lower()}.json'
else:
    path = Path(f'output{os.sep}{affix.upper()}_with_additions')
    data_fn = path / f'Google_{affix.lower()}_cleaned.json'

path_scraped_pages = Path(f'output{os.sep}{affix.upper()}_with_additions{os.sep}scraped_webpages')
print('Reading data input...')
df = pd.read_json(data_fn, lines=True)
df_names = pd.read_json(f'input{os.sep}common_names.json', lines=True)

if subsample_rows > 0:
    df = df.head(subsample_rows)
df.reset_index(inplace=True)

# Adding relevance columns
df.insert(len(df.columns), 'name_found', "")
df.insert(len(df.columns), 'relevance', 0.0)
df.insert(len(df.columns), 'relevance_names', False)
for term in relevance_filter_terms:
    df.insert(len(df.columns), 'relevance_'+term, False)


# Add 'common_name', 'scientific_name' columns
def common_name_preproc(row):
    return df_names.loc[row.id-1, 'common_name']


def scientific_name_preproc(row):
    return df_names.loc[row.id-1, 'scientific_name']


if 'common_name' not in df.columns:
    df.insert(1, 'common_name', "")
    df.insert(2, 'scientific_name', "")
    print('Adding common name column...')
    df.common_name = df.progress_apply(common_name_preproc, axis=1)
    print()
    print('Adding scientific name column...')
    df.scientific_name = df.progress_apply(scientific_name_preproc, axis=1)


# Searching for names and relevance_filter_terms
def search_for_names_and_filter_terms(row):
    scraped_fname = path_scraped_pages / (str(row['index']) + '.txt')
    for col in columns_to_search:
        if col == 'scraped_webpage':
            col_data = ''
            if scraped_fname.is_file():
                with open(scraped_fname, 'r', encoding='utf-8') as text_file:
                    col_data = text_file.read()
        else:
            col_data = row[col]
        if col_data is not None:
            col_data = col_data.lower()

            for flt_term in relevance_filter_terms:
                if flt_term in col_data:
                    row['relevance_'+flt_term] = True

            for nm in df_names.loc[row.id-1, 'all_names']:
                if nm != "" and nm in col_data:
                    row.relevance_names = True
                    row.name_found = nm
    return row


print()
print('Searching for names and filter terms...')
df = df.progress_apply(search_for_names_and_filter_terms, axis=1)

# Calculating relevance scores
filter_terms_weight = 0.5/len(relevance_filter_terms)


def calculate_scores(row):
    score = row['relevance_names'] * 0.5
    for term in relevance_filter_terms:
        score += row['relevance_'+term] * filter_terms_weight
    row['relevance'] = score
    return row


print()
print('Calculating relevance scores...')
df = df.progress_apply(calculate_scores, axis=1)

# Saving data
dirName = path/'filtered'
dirName.mkdir(parents=True, exist_ok=True)
fname = path / 'filtered' / f'Google_{affix.lower()}_filtered'
print('Saving data...')
df.to_json(fname.with_suffix('.json'), orient='records', lines=True, force_ascii=False)

# Saving the csv with special character separator like ζ works well with even non-english commonly used text
df = df.applymap(
    lambda x: str(x).replace('\n', ' '))  # Replace newlines to not affect importing them in Spreadsheets
df = df.applymap(lambda x: str(x).replace('\r', ' '))
df.to_csv(fname.with_suffix('.csv'), encoding='utf-8-sig', sep='ζ', index=False)



print()
