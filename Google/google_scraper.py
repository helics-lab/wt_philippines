import json
import sys
from pathlib import Path
import pandas as pd
import re
import math
import random
import string
from googleapiclient.discovery import build
import argparse
from utilities import *

# System call to enable colored text display in Windows terminal
os.system("")

parser = argparse.ArgumentParser()
parser.add_argument('--row_start', '-s', dest='row_start', type=int, default=1,
                    help='start scraping at a specified '
                         'row number (default: 1) from the ADB names list csv file in the input folder', required=False)
parser.add_argument('--row_end', '-e', dest='row_end', type=int, default=163,
                    help='end scraping at the specified '
                         'row number [not including the ROW_END] (default: 163) from the ADB names list csv file in '
                         'the input folder',
                    required=False)
parser.add_argument('--pageLimit', '-p', dest='pageLimit', type=int, default=10,
                    help='How many pages to read from a search result (each with 10 results)', required=False)
parser.add_argument('--country', '-c', dest='country', type=str, default=None,
                    help='country parameter for the Google search API to restrict search for only specific country or '
                         'all the world. If search for only Philippines (--country countryPH). If search needed for '
                         'all the world ignore this argument or pass None. Default: None. For all possible values, '
                         'see: https://metacpan.org/pod/WWW::Google::CustomSearch#Country-Collection-Values-(cr)',
                    required=False)
parser.add_argument('--affix', '-a', dest='affix', type=str,
                    help='affix for saving filenames and data. Example if Philippines restricted area is needed then '
                         'affix can be ph or PH. This argument is required.',
                    required=True)
parser.add_argument('--delete_previous_versions', '-del', dest='delete_previous_versions',
                    type=lambda s: s.lower() in ['true', 't', 'yes', '1'], default=True,
                    help='Delete previous redundant data scraped files (default: True).', required=False)

args = parser.parse_args()

row_start = args.row_start
row_end = args.row_end  # not including
pageLimit = args.pageLimit  # How many pages to read from a search result (each with 10 results).
delete_previous_versions = args.delete_previous_versions  # delete previous redundant data scraped files
country = args.country
affix = args.affix
search_attempts = 0
search_page_attempts = 0

pd_input = pd.read_csv("input" + os.sep + "names.csv")
path = f'output{os.sep}{affix.upper()}_with_additions{os.sep}'
Path(path).mkdir(parents=True, exist_ok=True)

fn = f'data_{affix.lower()}'
prev_file_names = []

row_start = row_start - 1  # convert ids into row numbers
row_end = row_end - 1  # not including

st_additions = ['', 'sell', 'sale', 'trade', 'USD', 'peso', 'cost', 'price',
                'Alibaba', 'reddit', 'Amazon', 'Lazada', 'Shopee', 'Carousell', 'Ebay',
                'ibenta', 'pagbebenta', 'kalakal', 'gastos', 'presyo']


def getService():
    with open('credentials.json') as file:
        credentials = json.load(file)

    KEY = credentials['Google_API_KEY']
    service = build("customsearch", "v1", developerKey=KEY)
    return service


def save_data(ids, search_terms, total_results_counts, titles, links, snippets, WebPage_inLanguage_list,
              NewsArticle_datePublished_list, NewsArticle_dateCreated_list, NewsArticle_dateModified_list,
              NewsArticle_itemtype_list, NewsArticle_description_list, NewsArticle_inLanguage_list,
              person_list, organization_list, metatags_medium_list, metatags_article_publisher_list,
              metatags_site_name_list, metatags_twitter_site_list, metatags_twitter_description_list,
              metatags_description_list, metatags_news_keyword_list, metatags_pubdate_list, metatags_lastmod_list,
              metatags_locale_list, metatags_locale_alternate_list, metatags_author_list, metatags_image_alt_list,
              metatags_article_tags, metatags_article_tag,
              article_articlebody_list, article_inlanguage_list, response_list, ids_for_response,
              search_terms_for_response,
              path, fn, row_start, row_end, delete_previous_versions=False):
    global prev_file_names

    data_fn = path + fn + '_' + str(row_start) + '-' + str(row_end) + '.csv'
    data_fn = find_next_filename(data_fn)  # If exists add number to the file name to prevent overwrite

    data_fn_json = path + fn + '_' + str(row_start) + '-' + str(row_end) + '.json'
    data_fn_json = find_next_filename(data_fn_json)  # If exists add number to the file name to prevent overwrite

    data_response_fn = path + fn + '_response_' + str(row_start) + '-' + str(row_end) + '.json'
    data_response_fn = find_next_filename(
        data_response_fn)  # If exists add number to the file name to prevent overwrite

    data = list(zip(ids, search_terms, total_results_counts, titles, links, snippets, WebPage_inLanguage_list,
                    NewsArticle_datePublished_list, NewsArticle_dateCreated_list, NewsArticle_dateModified_list,
                    NewsArticle_itemtype_list, NewsArticle_description_list, NewsArticle_inLanguage_list,
                    person_list, organization_list, metatags_medium_list, metatags_article_publisher_list,
                    metatags_site_name_list, metatags_twitter_site_list, metatags_twitter_description_list,
                    metatags_description_list, metatags_news_keyword_list, metatags_pubdate_list, metatags_lastmod_list,
                    metatags_locale_list, metatags_locale_alternate_list, metatags_author_list, metatags_image_alt_list,
                    metatags_article_tags, metatags_article_tag,
                    article_articlebody_list, article_inlanguage_list))
    data_response = list(zip(ids_for_response, search_terms_for_response, response_list))
    columns = ['id', 'search term', 'total results count', 'title', 'link', 'snippet', 'WebPage_inLanguage',
               'NewsArticle_datePublished', 'NewsArticle_dateCreated', 'NewsArticle_dateModified',
               'NewsArticle_itemtype', 'NewsArticle_description', 'NewsArticle_inLanguage',
               'person_dict', 'organization_dict', 'metatags_medium', 'metatags_article_publisher',
               'metatags_site_name', 'metatags_twitter_site', 'metatags_twitter_description',
               'metatags_description', 'metatags_news_keyword', 'metatags_pubdate',
               'metatags_lastmod',
               'metatags_locale', 'metatags_locale_alternate', 'metatags_author',
               'metatags_image_alt', 'metatags_article_tags', 'metatags_article_tag',
               'article_articlebody', 'article_inlanguage']
    df = pd.DataFrame(data, columns=columns)
    df.to_json(data_fn_json, orient='records', lines=True, force_ascii=False)

    # Saving the csv with special character separator like ζ works well with even non-english commonly used text
    df = df.applymap(
        lambda x: str(x).replace('\n', ' '))  # Replace newlines to not affect importing them in Spreadsheets
    df = df.applymap(lambda x: str(x).replace('\r', ' '))
    df.to_csv(data_fn, encoding='utf-8-sig', sep='ζ', index=False)

    df_response = pd.DataFrame(data_response, columns=['id', 'search term', 'response_list'])
    df_response.to_json(
        data_response_fn)
    print(f'{bcolors.OKBLUE}Search results saved in: {data_fn}  ; {data_fn_json} ; {data_response_fn}{bcolors.ENDC}')

    if delete_previous_versions:
        print()
        for nm in prev_file_names:
            if os.path.exists(nm):
                os.remove(nm)
                print(f'{bcolors.WARNING}Deleted previous file version: {nm}{bcolors.ENDC}')
        print()
    prev_file_names = [data_fn, data_fn_json, data_response_fn]


service = getService()

ids, titles, links, snippets, search_terms, total_results_counts = [], [], [], [], [], []
WebPage_inLanguage_list, NewsArticle_datePublished_list, \
NewsArticle_dateCreated_list, NewsArticle_dateModified_list, NewsArticle_itemtype_list, \
NewsArticle_description_list = [], [], [], [], [], []
NewsArticle_inLanguage_list, person_list, organization_list, metatags_medium_list, \
metatags_article_publisher_list = [], [], [], [], []
metatags_site_name_list, metatags_twitter_site_list, metatags_twitter_description_list, \
metatags_description_list, article_inlanguage_list = [], [], [], [], []
metatags_news_keyword_list, metatags_pubdate_list, metatags_lastmod_list, metatags_locale_list = [], [], [], []
metatags_locale_alternate_list, metatags_author_list, metatags_image_alt_list, article_articlebody_list = [], [], [], []
metatags_article_tags_list, metatags_article_tag_list = [], []
response_list, ids_for_response_list, search_terms_for_response_list, prev_file_names = [], [], [], []

for row in range(row_start,
                 row_end):
    for species_name in [pd_input.english_name[row], pd_input.scientific_name[row], pd_input.local_name[row]]:
        if isinstance(species_name, float):
            if math.isnan(species_name): continue  # exclude empty cells

        nms = re.sub('\\\|\.|\\""|\:|�|\)', '', species_name)
        for st in re.split(';|,|\/|\(', nms):
            search_term_pure = st.strip()  # remove trailing and leading spaces
            for addition in st_additions:
                if addition == '':
                    search_term = search_term_pure
                else:
                    search_term = search_term_pure + ' ' + addition

                search_attempts += 1
                response = []
                startIndex = 1
                print()
                print(f'{bcolors.OKBLUE}Searching for: {row + 1}. {search_term}{bcolors.ENDC}')  # id. search_term
                print()
                for nPage in range(0, pageLimit):
                    print(f'{bcolors.OKGREEN}Google search attempt: {search_attempts}{bcolors.ENDC}')
                    print(
                        f"Retrieving results from Google for the search term: {search_term} ; page number: {nPage + 1}")
                    userID = ''.join(random.choices(string.ascii_lowercase + string.digits, k=38))
                    try:
                        search_page_attempts += 1
                        print(f'Search subpage attempt: {search_page_attempts}')
                        response.append(service.cse().list(
                            q=search_term,  # Search words
                            cr=country,
                            cx='001132580745589424302:jbscnf14_dw',
                            quotaUser=userID,
                            start=startIndex
                        ).execute())
                    except Exception as e:
                        if e.status_code == 400:
                            print(
                                f'{bcolors.WARNING}The response of Google was with HttpError 400: Request contains an '
                                f'invalid argument. Some pages are ended and no next page available even though prev. '
                                f'page says so.. Error will be ignored.{bcolors.ENDC}')
                            break  # No next page available in the Google search results (i.e., this is the last page)
                        else:
                            print()
                            print(
                                f'{bcolors.FAIL}An exception occurred in the Custom Search API. '
                                f'Here is the exception:{bcolors.ENDC}')
                            print(f'{bcolors.WARNING}{e}{bcolors.ENDC}')
                            print()
                            print(f'{bcolors.OKCYAN}Here is the last API response:{bcolors.ENDC} ')
                            print(f'{bcolors.OKBLUE}{response}{bcolors.ENDC}')
                            sys.exit()

                    if 'nextPage' in response[nPage]['queries']:
                        startIndex = response[nPage].get("queries").get("nextPage")[0].get("startIndex")
                    else:
                        break  # No next page available in the Google search results (i.e., this is the last page)

                response_list.append(response)
                ids_for_response_list.append(int(pd_input.id[row]))
                search_terms_for_response_list.append(search_term)

                # Reading Search Results
                for page in response:
                    link, title, snippet, pagemap, WebPage_inLanguage = '', '', '', '', ''
                    NewsArticle_datePublished, NewsArticle_dateCreated, NewsArticle_dateModified = '', '', ''
                    NewsArticle_itemtype, NewsArticle_description, NewsArticle_inLanguage = '', '', ''
                    person, organization = '', ''
                    metatags_medium, metatags_article_publisher, metatags_site_name = '', '', ''
                    metatags_twitter_site, metatags_twitter_description, metatags_description = '', '', ''
                    metatags_news_keyword, metatags_pubdate, metatags_lastmod = '', '', ''
                    metatags_locale, metatags_locale_alternate, metatags_author, metatags_image_alt = '', '', '', ''
                    metatags_article_tags, metatags_article_tag = '', ''
                    article_articlebody, article_inlanguage = '', ''

                    if 'totalResults' in page['queries']['request'][0]:
                        total_results_count = page['queries']['request'][0][
                            'totalResults']
                    if 'items' in page:
                        for item in page['items']:
                            link = item['link']
                            if 'title' in item:
                                title = item['title']

                            if 'snippet' in item:
                                snippet = item['snippet']

                            if 'pagemap' in item:
                                pagemap = item['pagemap']
                                if 'WebPage' in pagemap:
                                    if 'inLanguage' in pagemap['WebPage'][0]:
                                        WebPage_inLanguage = pagemap['WebPage'][0]['inLanguage']

                                if 'NewsArticle' in pagemap:
                                    if 'datePublished' in pagemap['NewsArticle'][0]:
                                        NewsArticle_datePublished = pagemap['NewsArticle'][0]['datePublished']

                                    if 'dateCreated' in pagemap['NewsArticle'][0]:
                                        NewsArticle_dateCreated = pagemap['NewsArticle'][0]['dateCreated']

                                    if 'dateModified' in pagemap['NewsArticle'][0]:
                                        NewsArticle_dateModified = pagemap['NewsArticle'][0]['dateModified']

                                    if 'itemtype' in pagemap['NewsArticle'][0]:
                                        NewsArticle_itemtype = pagemap['NewsArticle'][0]['itemtype']

                                    if 'description' in pagemap['NewsArticle'][0]:
                                        NewsArticle_description = pagemap['NewsArticle'][0]['description']

                                    if 'inLanguage' in pagemap['NewsArticle'][0]:
                                        NewsArticle_inLanguage = pagemap['NewsArticle'][0]['inLanguage']

                                if 'person' in pagemap:
                                    person = pagemap['person']

                                if 'organization' in pagemap:
                                    organization = pagemap['organization'][0]
                                elif 'Organization' in pagemap:
                                    organization = pagemap['Organization'][0]

                                if 'metatags' in pagemap:
                                    if 'medium' in pagemap['metatags'][0]:
                                        metatags_medium = pagemap['metatags'][0]['medium']

                                    if 'article:publisher' in pagemap['metatags'][0]:
                                        metatags_article_publisher = pagemap['metatags'][0]['article:publisher']

                                    if 'og:site_name' in pagemap['metatags'][0]:
                                        metatags_site_name = pagemap['metatags'][0]['og:site_name']

                                    if 'twitter:site' in pagemap['metatags'][0]:
                                        metatags_twitter_site = pagemap['metatags'][0]['twitter:site']

                                    if 'twitter:description' in pagemap['metatags'][0]:
                                        metatags_twitter_description = pagemap['metatags'][0]['twitter:description']

                                    if 'og:description' in pagemap['metatags'][0]:
                                        metatags_description = pagemap['metatags'][0]['og:description']

                                    if 'news_keywords' in pagemap['metatags'][0]:
                                        metatags_news_keyword = pagemap['metatags'][0]['news_keywords']

                                    if 'pubdate' in pagemap['metatags'][0]:
                                        metatags_pubdate = pagemap['metatags'][0]['pubdate']

                                    if 'lastmod' in pagemap['metatags'][0]:
                                        metatags_lastmod = pagemap['metatags'][0]['lastmod']

                                    if 'og:locale' in pagemap['metatags'][0]:
                                        metatags_locale = pagemap['metatags'][0]['og:locale']

                                    if 'og:locale:alternate' in pagemap['metatags'][0]:
                                        metatags_locale_alternate = pagemap['metatags'][0]['og:locale:alternate']

                                    if 'author' in pagemap['metatags'][0]:
                                        metatags_author = pagemap['metatags'][0]['author']

                                    if 'og:image:alt' in pagemap['metatags'][0]:
                                        metatags_image_alt = pagemap['metatags'][0]['og:image:alt']

                                    if 'article-tags' in pagemap['metatags'][0]:
                                        metatags_article_tags = pagemap['metatags'][0]['article-tags']

                                    if 'article:tag' in pagemap['metatags'][0]:
                                        metatags_article_tag = pagemap['metatags'][0]['article:tag']

                                if 'article' in pagemap:
                                    if 'articlebody' in pagemap['article'][0]:
                                        article_articlebody = pagemap['article'][0]['articlebody']

                                    if 'inlanguage' in pagemap['article'][0]:
                                        article_inlanguage = pagemap['article'][0]['inlanguage']

                            titles.append(title)
                            links.append(link)
                            snippets.append(snippet)
                            search_terms.append(search_term)
                            total_results_counts.append(total_results_count)
                            WebPage_inLanguage_list.append(WebPage_inLanguage)
                            NewsArticle_datePublished_list.append(NewsArticle_datePublished)
                            NewsArticle_dateCreated_list.append(NewsArticle_dateCreated)
                            NewsArticle_dateModified_list.append(NewsArticle_dateModified)
                            NewsArticle_itemtype_list.append(NewsArticle_itemtype)
                            NewsArticle_description_list.append(NewsArticle_description)
                            NewsArticle_inLanguage_list.append(NewsArticle_inLanguage)
                            person_list.append(person)
                            organization_list.append(organization)
                            metatags_medium_list.append(metatags_medium)
                            metatags_article_publisher_list.append(metatags_article_publisher)
                            metatags_site_name_list.append(metatags_site_name)
                            metatags_twitter_site_list.append(metatags_twitter_site)
                            metatags_twitter_description_list.append(metatags_twitter_description)
                            metatags_description_list.append(metatags_description)
                            metatags_news_keyword_list.append(metatags_news_keyword)
                            metatags_pubdate_list.append(metatags_pubdate)
                            metatags_lastmod_list.append(metatags_lastmod)
                            metatags_locale_list.append(metatags_locale)
                            metatags_locale_alternate_list.append(metatags_locale_alternate)
                            metatags_author_list.append(metatags_author)
                            metatags_image_alt_list.append(metatags_image_alt)
                            metatags_article_tags_list.append(metatags_article_tags)
                            metatags_article_tag_list.append(metatags_article_tag)
                            article_articlebody_list.append(article_articlebody)
                            article_inlanguage_list.append(article_inlanguage)

                            ids.append(int(pd_input.id[row]))
                    else:
                        print(f'{bcolors.WARNING}No items found in this Google result page!{bcolors.ENDC}')
                        pass

    save_data(ids, search_terms, total_results_counts, titles, links, snippets, WebPage_inLanguage_list,
              NewsArticle_datePublished_list, NewsArticle_dateCreated_list, NewsArticle_dateModified_list,
              NewsArticle_itemtype_list, NewsArticle_description_list, NewsArticle_inLanguage_list,
              person_list, organization_list, metatags_medium_list, metatags_article_publisher_list,
              metatags_site_name_list, metatags_twitter_site_list, metatags_twitter_description_list,
              metatags_description_list, metatags_news_keyword_list, metatags_pubdate_list, metatags_lastmod_list,
              metatags_locale_list, metatags_locale_alternate_list, metatags_author_list, metatags_image_alt_list,
              metatags_article_tags_list, metatags_article_tag_list,
              article_articlebody_list, article_inlanguage_list, response_list, ids_for_response_list,
              search_terms_for_response_list,
              path, fn, row_start + 1, row + 2, delete_previous_versions)
