from selenium import webdriver
import time
from inscriptis import get_text
import joblib
from joblib import Parallel, delayed
import pandas as pd
import numpy as np
from tqdm import tqdm
from pathlib import Path
from fake_useragent import UserAgent
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument('--affix', '-a', dest='affix', type=str,
                    help='affix for saving filenames and data. Example if Philippines restricted area is needed then '
                         'affix can be ph or PH. This argument is required.', required=True)

parser.add_argument('--retry_skipped_downloads', '-r', dest='retry_skipped_downloads',
                    type=lambda s: s.lower() in ['true', 't', 'yes', '1'], default=True,
                    help='Retry loading webpages skipped to load in a previous run (default: True)', required=False)
parser.add_argument('--max_scroll_tries', '-m', dest='max_scroll_tries', type=int, default=5,
                    help='maximum number of scroll to end swipes to let the JS code running on all the webpage to '
                         'load data (default: 5)', required=False)
parser.add_argument('--workers', '-w', dest='workers', type=int, default=joblib.cpu_count()*2,
                    help='number of workers running in parallel (default: 2*number of cpu cores of this machine)',
                    required=False)

args = parser.parse_args()

affix = args.affix
RETRY_SKIPPED_DOWNLOAD = args.retry_skipped_downloads  # Retry loading webpages skipped to load in a previous run
SUBSAMPLE_ROWS = 0  # 0 for all rows
MAX_SCROLL_TRIES = args.max_scroll_tries
WORKERS = args.workers


##################### NOTES #####################
# * The cache, debug files and other browser related files related to the browsed websites
# of chrome driver browser when build up will cause HTTPConnection errors like:
# NewConnectionError('<urllib3.connection.HTTPConnection object at 0x0 ....
# Errors will start when this reaches like > 10GB and >250K files even if there is plenty of free storage
# This will make the script skip the file and save .skp file (which can be retired to download later)
# So better from time to time to delete the temp files which is on windows in:
# C:\Users\<USER>\AppData\Local\Temp

# * Currently (Aug 2021, Selenium 4 is in beta version and Stable is ver 3,
# Check for the latest version at: https://www.selenium.dev/downloads/
# to explore the newest feature (but not required for this project): pip install selenium==4.0.0.b4

# * Download the latest chromeDriver and put it in the project folder
# https://sites.google.com/a/chromium.org/chromedriver/downloads

# * List of Chromium options and arguments:
# https://stackoverflow.com/questions/38335671/where-can-i-find-a-list-of-all-available-chromeoption-arguments

file_types_excluded = ['3GP', '7Z', 'AAC', 'ACE', 'AIF', 'ARJ', 'ASF', 'AVI', 'BIN', 'BZ2', 'DOC', 'DOCX', 'EXE', 'GZ',
                       'GZIP', 'IMG', 'ISO', 'LZH', 'M4A', 'M4V', 'MKV', 'MOV', 'MP3', 'MP4', 'MPA', 'MPE', 'MPEG',
                       'MPG', 'MSI', 'MSU', 'OGG', 'OGV', 'PDF', 'PLJ', 'PPS', 'PPT', 'QT', 'RA', 'RAR',
                       'RM', 'RMVB', 'SEA', 'SIT', 'SITX', 'TAR', 'TIF', 'TIFF', 'WAV', 'WMA', 'WMV', 'Z', 'ZIP']

def scrape_pd_links(df, path_save, n_process):
    options = webdriver.ChromeOptions()
    ua = UserAgent().random
    options.add_argument("--disable-blink-features")
    options.add_argument("--disable-blink-features=AutomationControlled")
    options.add_argument("ignore-certificate-errors")
    options.add_argument("--no-sandbox")
    options.add_argument("disable-notifications")
    options.add_argument("--disable-infobars")
    options.add_argument("--disable-extensions")
    options.add_argument(f'user-agent={ua}')
    options.add_argument("--aggressive-cache-discard")
    options.add_argument("--media-cache-size=0")  # minimize cache saving
    options.add_argument("--disk-cache-size=0")

    # Avoid some websites stuck selenium and the whole process stuck in loading the page
    capabilities = webdriver.DesiredCapabilities.CHROME.copy()
    capabilities['pageLoadStrategy'] = "none"  # default is "normal". See: https://stackoverflow.com/a/42403135/1970830  and  https://github.com/bayandin/chromedriver/blob/e9a1f55b166ea62ef0f6e78da899d9abf117e88f/chrome/page_load_strategy.h
    capabilities['acceptInsecureCerts'] = True
    with webdriver.Chrome(options=options, desired_capabilities=capabilities) as driver:
        def scrape_link(link):
            if link.split('.')[-1].upper() in file_types_excluded:
                return ""
            try:
                driver.get(link)
            except Exception as e:
                print()
                print(f"This link could not be loaded: {link}")
                print(str(e))
                if "Timed out" in str(e) or "TIMED_OUT" in str(e):
                    pass
                else:
                    return ""

            # Loop to scroll down to end of the page to let all javascript code load its content
            # JS script that scrolls to a tenth of the page's height, pauses for 500 milliseconds, and then continues
            js_scroller = """
                var scroll = document.body.scrollHeight / 10;
                var i = 0;
                function scrollit(i) {
                   window.scrollBy({top: scroll, left: 0, behavior: 'smooth'});
                   i++;
                   if (i < 10) {
                       setTimeout(scrollit, 500, i);
                   }
                }
                scrollit(i);
                return document.body.scrollHeight;
            """
            lenOfPage = 0
            try:
                lenOfPage = driver.execute_script(js_scroller)
            except:
                pass
            match = False
            tries = 0
            while (match == False):
                lastCount = lenOfPage
                time.sleep(6)  # Cloudflare waits for 5 sec before redirecting to the real website
                try:
                    lenOfPage = driver.execute_script(js_scroller)
                except:
                    pass
                tries += 1
                if lastCount == lenOfPage or tries > MAX_SCROLL_TRIES:
                    match = True
            txt = ""
            try:
                txt = get_text(driver.page_source)
            except Exception as e:
                print(f'Could not read the text from the page: {link} with the error: {str(e)}')
            return txt

        def save_scraped_text(txt, fname):
            fname = path_save / fname
            with open(fname, "w", encoding="utf-8") as text_file:
                text_file.write(txt)

        text = f"progresser #{n_process}"
        for idx in tqdm(df.index, desc=text, position=n_process):
            row = df.loc[idx]
            url = row.link
            text = scrape_link(url)
            # delete whatever can be deleted of the browser's files saved at local storage
            try:
                driver.execute_script('window.localStorage.clear()')
            except:
                pass
            try:
                driver.execute_script('window.sessionStorage.clear()')
            except:
                pass
            try:
                driver.execute_cdp_cmd("Network.clearBrowserCache", {})
            except:
                pass
            try:
                driver.delete_all_cookies()
            except:
                pass

            if text != "":
                old_skp = path_save / (str(idx) + '.skp')
                try:
                    old_skp.unlink()
                except:
                    pass
                suffix = ".txt"
            else:
                suffix = ".skp"
            save_scraped_text(text, str(idx)+suffix)
        driver.quit()


if __name__ == '__main__':
    """joblib docs contain the following warning: 
        Under Windows, it is important to protect the main loop of code to avoid recursive 
        spawning of subprocesses when using joblib.Parallel.
        So put the main loop of code under __main__"""

    path = Path(f'output{os.sep}{affix.upper()}_with_additions')

    name = f'Google_{affix.lower()}_cleaned.json'
    data_fn = path / name
    path_save = path / 'scraped_webpages'
    path_save.mkdir(parents=True, exist_ok=True)

    print('Reading data input...')
    df = pd.read_json(data_fn, lines=True)

    if SUBSAMPLE_ROWS > 0:
        df = df.head(SUBSAMPLE_ROWS)
    df.reset_index(inplace=True)

    # Exclude already scraped webpages
    print()
    fname_list = path_save.glob('*.txt')
    to_drop_idx = [int(fn.stem) for fn in fname_list]
    print(f"The total of previously scraped webpages = {len(to_drop_idx)}")
    df.drop(to_drop_idx, axis=0, inplace=True)

    # Exclude previously skipped webpages due to loading error or excluded filetypes
    print()
    if not RETRY_SKIPPED_DOWNLOAD:
        fname_list = path_save.glob('*.skp')
        to_drop_idx = [int(fn.stem) for fn in fname_list]
        print(f"The total of previously skipped webpages = {len(to_drop_idx)}")
        df.drop(to_drop_idx, axis=0, inplace=True)

    print()
    print(f"Start scraping {len(df)} webpages, using {WORKERS} parallel processes, "
          f"on a machine with {joblib.cpu_count()} CPU cores.")
    df_split = np.array_split(df, WORKERS, axis=0)

    delayed_funcs = [delayed(scrape_pd_links)(df, path_save, n) for n, df in enumerate(df_split)]
    parallel_pool = Parallel(n_jobs=WORKERS, verbose=10)
    parallel_pool(delayed_funcs)
    print('Finished all jobs...')
