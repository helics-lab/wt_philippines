import pandas as pd
import os
import csv
from tqdm import tqdm
import argparse

csv.field_size_limit(13107200)


parser = argparse.ArgumentParser()
parser.add_argument('--affix', '-a', dest='affix', type=str,
                    help='affix for saving filenames and data. Example if Philippines restricted area is needed then '
                         'affix can be ph or PH. This argument is required.', required=True)
args = parser.parse_args()
affix = args.affix
path = f'output{os.sep}{affix.upper()}_with_additions{os.sep}'
fname = f"{path}Google_{affix.lower()}_cleaned"
data_fn = f"{path}data_{affix.lower()}_all"

print('Reading data input...')
df = pd.read_json(data_fn+'.json', lines=True)
df.reset_index(drop=True, inplace=True)

# Cleaning: removing duplicated links per species id
print('Cleaning from duplicated links per species id...')
df_cleaned = pd.DataFrame(columns=df.columns)
for id in tqdm(range(1, 158)):
    df_temp = df[df.id == id].copy()
    df_temp.drop_duplicates(subset=['link'], keep='first', inplace = True, ignore_index= True)
    df_cleaned = pd.concat([df_cleaned, df_temp], axis=0, ignore_index=True)


print('Saving data...')
df_cleaned.to_json(fname + '.json', orient='records', lines=True, force_ascii=False)

# Saving the csv with special character separator like ζ works well with even non-english commonly used text
# Replace newlines to not affect importing them in Spreadsheets
df_cleaned = df_cleaned.applymap(lambda x: str(x).replace('\n', ' '))
df_cleaned = df_cleaned.applymap(lambda x: str(x).replace('\r', ' '))
df_cleaned.to_csv(fname + '.csv', encoding='utf-8-sig', sep='ζ', index=False)

print(f"Google cleaned file saved to {fname}")
