import os
import requests
import exifread
from PIL import Image
from pathlib import Path
import reverse_geocode as rg


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def download_img(uri, filename):
    download_success = True
    tags = {}
    with open(filename, 'wb') as file:
        try:
            file.write(requests.get(uri, timeout=120).content)
        except Exception as e:
            download_success = False
            print()
            print(
                f'{bcolors.BOLD}{bcolors.FAIL}Failed to download the file named {filename} with uri: {uri}{bcolors.ENDC}')
            print()
            return download_success, tags

    # Add suffix
    if Path(filename).suffix == '':
        filename_old = filename
        filename += '.' + Image.open(filename).format.lower()
        os.rename(filename_old, filename)

    # Extract EXIF info from the image
    # https://pypi.org/project/exif/
    # Returned tags will be a dictionary mapping names of Exif tags to their values
    with open(filename, 'rb') as file:
        try:
            tags = exifread.process_file(file)
        except Exception as e:
            print()
            print(
                f'{bcolors.WARNING}Failed to read the Exif data of the file named {filename} with the uri: {uri}{bcolors.ENDC}')
            print()
            pass
        return download_success, tags


def reverse_geocode(coordinates):
    return rg.search(coordinates)
