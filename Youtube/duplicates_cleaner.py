import pandas as pd
import os
import csv
from tqdm import tqdm

csv.field_size_limit(13107200)
path = 'output' + os.sep
data_fn = path + 'data_all.json'

print('Reading data input...')
df = pd.read_json(data_fn, lines=True)
df.reset_index(drop=True, inplace=True)

# Cleaning: removing duplicated links per species id
print('Cleaning from duplicated links per species id...')
df_cleaned = pd.DataFrame(columns=df.columns)
for id in tqdm(range(1, 158)):
    df_temp = df[df.id == id].copy()
    df_temp.drop_duplicates(subset=['youtube_link'], keep='first', inplace = True, ignore_index=True)
    df_cleaned = pd.concat([df_cleaned, df_temp], axis=0, ignore_index=True)

fname = "output/Youtube_cleaned"

print('Saving data...')
df_cleaned.to_json(fname + '.json', orient='records', lines=True, force_ascii=False)

# Saving the csv with special character separator like ζ works well with even non-english commonly used text
# Replace newlines to not affect importing them in Spreadsheets
df_cleaned = df_cleaned.applymap(lambda x: str(x).replace('\n', ' '))
df_cleaned = df_cleaned.applymap(lambda x: str(x).replace('\r', ' '))
df_cleaned.to_csv(fname + '.csv', encoding='utf-8-sig', sep='ζ', index=False)

print(f"Twitter cleaned file saved to {fname}")
