#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 22 20:24:42 2020

@author: rtwik
"""

import os
import pandas as pd
import requests
from bs4 import BeautifulSoup
import json
from datetime import datetime
from pymongo import MongoClient
from joblib import Parallel, delayed
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import numpy
from data_processor import Preprocessor
from model_functions import Model_functions
import spacy
from langdetect import detect
from pygooglenews import GoogleNews


class Data_Processor(object):
    '''methods to process data'''
    
    def __init__(self):
        pass
    
    def get_keywords_from_cites(self, cites_csv, col_name):
        '''input: csv file dowloaded from the CITES webpage
           returns: data frame of keywords from col_name and scientific name as reference column'''
           
        return cites_csv[['FullName', col_name, 'All_DistributionFullNames', 'FullAnnotationEnglish']].dropna()


    def format_google_query(self, query_list, query_type='str'):
        '''convert keywords into google query web address format for exact phrase match'''
                    
        if query_type == 'str':
            keywords_format =  ['\"' + '%20'.join(q.split()) + '\"' for q in query_list]
            URL = 'https://news.google.com/search?for={}&hl=en-US&gl=US&ceid=US%3Aen'
        elif query_type == 'query':
            keywords_format =  ['%20'.join(q.split()) for q in query_list]
            URL = 'https://news.google.com/search?q={}&hl=en-US&gl=US&ceid=US%3Aen'
        else:
            print('error: query_type must be "str" or "q"')

        return [URL.format(q) for q in keywords_format]

    def make_google_queries(self, keyword_frame, col_name, keyword_source, query_type):
        '''returns dictionary with species scientific name as key and english names
           formatted into google search query URL. keyword_source can be a cites csv
           or a plain text (open); quety_type cant be a plain string or a logical google
           search query'''
           
        google_queries = []
        
        if keyword_source == 'cites':
            for index,row in keyword_frame.iterrows():
                k_words = [' '.join(l.split()) for l in row[col_name].split(',')]
                google_queries.append([row['FullName'],
                                      row['EnglishNames'],
                                      row['All_DistributionFullNames'],
                                      row['FullAnnotationEnglish'],
                                      self.format_google_query(k_words, query_type='str' )])
        elif keyword_source == 'open':
            k_words = [' '.join(l.split()) for l in keyword_frame]
            google_queries.append([keyword_frame[0], 'NA', 'NA',
                                  self.format_google_query(k_words, query_type)])
        else:
            print('error: source must be "cites" or "open" in make_google_queries()')
            raise SystemExit()
            
        query_data = pd.DataFrame(google_queries)
        query_data.columns = ['FullName', 'EnglishNames','Distribution', 'FullAnnotationEnglish', 'GoogleQuery']

        return query_data
    
    
    def remove_duplicates(self, data):
        ''' remove articles that repeat'''
        
        docs = [[k, data['articles'][k]['text']] for k in data['articles']]
        vectorizer = TfidfVectorizer()
        docs_tfidf = vectorizer.fit_transform([d[1] for d in docs])
        
        query_tfidf = vectorizer.transform([d[1] for d in docs])
    
        s = cosine_similarity(query_tfidf, docs_tfidf)
        s = s - numpy.identity(len(s))
        
        dup_inds = numpy.where(s >= 0.95)
    
        dup_inds = dup_inds[0][:int(len(dup_inds[0])/2)]  # only half the list due to symmetry
        
        #unique_inds = [j for j in range(len(docs)) if j not in dup_inds] 
        
        if len(dup_inds) > 0:
            keys = []
            for j in dup_inds:
                keys.append(docs[j][0])
                
            keys = set(keys)
            for k in keys:
                del data['articles'][k]
            
        return data
    
    def remove_blanks(self, data):
        ''' remove docs with blank text'''
        
        keys = [k for k in data['articles'] if len(data['articles'][k]['text'].split()) < 1]
        
        keys = set(keys)
        for k in keys:
            del data['articles'][k]
            
        return data
    
    def remove_non_english(self, data):
        ''' remove docs with blank text'''
        
        keys = [k for k in data['articles'] if detect(data['articles'][k]['text']) != 'en']
        
        keys = set(keys)
        for k in keys:
            del data['articles'][k]
            
        return data
    
    def initiate_model(self, model_params):
        '''load and initialise neural net model'''
        
        self.CURRENT_DIR = os.getcwd()
        self.MODELS_DIR = self.CURRENT_DIR + '/models/'
        
        self.MF = Model_functions()
        self.PR = Preprocessor(**model_params)
        
        self.model = self.MF.load_saved_model(**model_params)
    
    def remove_irrelevant(self, data, model_threshold):
        '''remove articles not related to topic based on neural net prediction'''
        
        articles = [(k, data['articles'][k]['text']) for k in data['articles']]

        art_text = [i[1] for i in articles]
        art_text = self.PR.make_text_for_vectorisation(art_text)
        vecs = self.PR.vectorize_data(art_text, [])
        predictions = self.model.predict(vecs[0])
        
        keys = numpy.array([i[0] for i in articles])
        inds = numpy.where(predictions > model_threshold)
        keys = keys[inds[0]]
        
        keys = set(keys)
        for k in keys:
            del data['articles'][k]
            
        return data
    


class Web_Scrape(object):
    '''methods to get text articles from web'''
    
    def __init__(self, database_name, collection_name, mongo_env='local'):
        
        if mongo_env == 'local':
            self.client = MongoClient('mongodb://localhost:27017/')
        else:
            mongo_docker_port = os.environ['MONGO_PORT_27017_TCP_ADDR'] + ':' + os.environ['MONGO_PORT_27017_TCP_PORT']
            self.client = MongoClient(mongo_docker_port)
        
        self.db = self.client[database_name]
        self.db_collection = self.db[collection_name]
        self.db_log = self.db['logs']
        self.seen_urls = self.get_seen_urls()
        self.DP = Data_Processor()
        self.iucn = pd.read_csv(os.getcwd() + '/data/iucn_status.csv')
        self.google_news = GoogleNews()
        
    def get_seen_urls(self):
        '''returns all the urls of articles stored in database'''
        return set([i['articles'][j]['link'] for i in self.db_collection.find() for j in i['articles']])
    
    def get_article_count(self):
        '''returns the total number of articles in dataset'''
        return sum([len(i['articles']) for i in self.db_collection.find()])
    
    def get_weblinks_from_URL(self, URL):
        '''scans for all hyperlinks from scraped content and selects the ones
           that are "most probably" news article links'''
           
        response = requests.get(URL)
        soup = BeautifulSoup(response.content, 'html.parser')
        weblinks = soup.find_all('a')
        links = [l.get('href') for l in weblinks if l.get('href')]
        
        return list(set([l for l in links if './articles' in l]))
    
    def parse_article(self, article_object, target_list):
        '''parse article to extract data (text, metadata). Checks words from
           filter list are present in article text'''
        
        try:
            article_object.parse()
            if any(w.lower() in article_object.text.lower() for w in target_list):
                if detect(article_object.text) == 'en':
                    date = article_object.publish_date
                    parsed_data = {'text': article_object.text, 
                                  'title': article_object.title,
                                  'link': article_object.url,
                                  'image': article_object.top_image,
                                  'date': date.strftime('%m/%d/%Y')}
                                
                return parsed_data
            else:
                print('no exact match')
                return 'None'
        
        except Exception:
            print('skipping')
            return 'None'
            
    def get_articles_from_link(self, links, target_list):
        '''iterate through the links and get all text if present from each
           of the links'''
           
        from newspaper import Article  # requires reimport at each call

        articles = {}
        cnt = 0
        for l in set(links):
            # url = head + l.replace(char_replace, '')
            url = self.get_full_url(l)
          
            if url not in self.seen_urls:
           
                try:
                    article = Article(url)
                    article.download()
                    
                    article_parsed = self.parse_article(article, target_list)
                    if isinstance(article_parsed, dict):
                        articles[str(cnt)] = article_parsed
                        self.seen_urls.add(url)
                except Exception:
                    print('can\'t download... skipping')
                
            cnt += 1
            print('done {} of {} links'.format(cnt, len(links)))

        return articles
    
    def save_json(self, obj, filepath):
        '''saves the scraped data in json format'''
        
        with open(filepath, 'w') as f:
                    json.dump(obj, f)
                    
    def get_iucn_status(self, scientific_name):
        '''returns the IUCN status of the species from the IUCN Red List'''
        
        if scientific_name in self.iucn['sci_name'].tolist():
            return self.iucn[self.iucn['sci_name']==scientific_name]['iucn_status'].iloc[0]
        else:
            return 'NA'

    
    def fetch_articles(self, query_data, search_engine='none', save_data=True):
        '''iterates through the query data frame, makes queries, searches links in
           the search results and extracts articles'. batch_num: use then large
           target list is split in multiple batches for parallel instances'''
           
        for index, row in query_data.iterrows():
            for eng_name in row['EnglishNames'].split(','):
                
                result = self.google_news.search(eng_name)
                links = [i['link'] for i in result['entries'] if 'link' in i]
                
                print('found {} links for {}: {}'.format(len(links), row['FullName'], eng_name))

                articles = self.get_articles_from_link(links, eng_name)
                sci_name = row['FullName']
                
                data_dict = {'common_name' : eng_name,
                             'scientific_name': sci_name,
                             'iucn_status': self.get_iucn_status(sci_name),
                             'cites_listing': row['FullAnnotationEnglish'],
                             'distribution': row['Distribution'],
                             'articles': articles,
                             'date_saved': datetime.today().strftime('%Y-%m-%d')
                             }
                                    
                if save_data:
                    self.db_collection.insert_one(data_dict)
                
        return self.db_collection.count_documents({})
    
    
    
    def get_full_url(self, short_url):
        try:
            session = requests.Session()  # so connections are recycled
            resp = session.head(short_url, allow_redirects=True, timeout=10)
            url = resp.url
            
            if 'url=' in url and '&ct' in url:
                return url.split('url=')[1].split('&ct')[0]
            else:
                return short_url
               
        except Exception:   
            return short_url

    def get_index_bounds(self, len_data, n_batch_split):
    
        inds = []
        for i in range(n_batch_split):
            ind_start = int(i * len_data/n_batch_split)
            ind_end = int((i+1) * len_data/n_batch_split)
            
            if ind_end + len_data/n_batch_split > len_data:
                ind_end = len_data
            
            inds.append((ind_start, ind_end))
            
        return inds
    
    def get_doc_ids(self):
        '''returns all the mongo doc ids'''
        
        return [ids['_id'] for ids in self.db_collection.find({})]
    
        
    def get_null_doc_ids(self):
        '''returns all the mongo doc ids with no articles'''
        
        return [ids['_id'] for ids in self.db_collection.find({}) if len(ids['articles'])<1]
    
    def deduplicate_db(self, doc_ids):
        '''removes articles which have the same main text content'''
        
        for doc_id in doc_ids:
            doc = self.db_collection.find_one({'_id': doc_id})
            doc = self.DP.remove_duplicates(doc)
            
            self.db_collection.replace_one({'_id': doc_id}, 
                                        {'common_name': doc['common_name'],
                                         'scientific_name': doc['scientific_name'], 'distribution': doc['distribution'],
                                         'iucn_status': doc['iucn_status'], 'articles': doc['articles'],
                                         'date_saved': datetime.today().strftime('%Y-%m-%d')})
            
    
    def remove_blank_db(self, doc_ids):
        '''removes articles if they are blank'''
        
        for doc_id in doc_ids:
            doc = self.db_collection.find_one({'_id': doc_id})
            doc = self.DP.remove_blanks(doc)
            
            self.db_collection.replace_one({'_id': doc_id}, 
                                        {'common_name': doc['common_name'],
                                         'scientific_name': doc['scientific_name'], 'distribution': doc['distribution'],
                                         'iucn_status': doc['iucn_status'], 'articles': doc['articles'],
                                         'date_saved': datetime.today().strftime('%Y-%m-%d')})
        
            
    def remove_null_entries(self, doc_ids):
        '''removes entire main document (species) if no articles are present'''
                
        for d in doc_ids:
            self.db_collection.delete_one({'_id': d})
            
    
    def language_filter(self, doc_ids):
        '''removes non English articles'''
        
        for doc_id in doc_ids:
            doc = self.db_collection.find_one({'_id': doc_id})
            doc = self.DP.remove_non_english(doc)
            
            self.db_collection.replace_one({'_id': doc_id}, 
                                        {'common_name': doc['common_name'],
                                         'scientific_name': doc['scientific_name'], 'distribution': doc['distribution'],
                                         'iucn_status': doc['iucn_status'], 'articles': doc['articles'],
                                         'date_saved': datetime.today().strftime('%Y-%m-%d')})
        
    
    def remove_irrelevant_db(self, doc_ids, model_params={}):
        '''removes articles from database if the fail model prediction critetia'''
        
        self.DP.initiate_model(model_params)
        
        for doc_id in doc_ids:
            doc = self.db_collection.find_one({'_id': doc_id})
            doc = self.DP.remove_irrelevant(doc, model_params['model_threshold'])
            
            self.db_collection.replace_one({'_id': doc_id}, 
                                        {'common_name': doc['common_name'],
                                          'scientific_name': doc['scientific_name'], 'distribution': doc['distribution'],
                                          'iucn_status': doc['iucn_status'], 'articles': doc['articles'],
                                          'date_saved': datetime.today().strftime('%Y-%m-%d')})
            
            
    def initiate_ner_model(self, model_name='en'):
        '''loads spacy model'''
        
        self.NLP = NER(model_name)
    
    def extract_ner_db(self, doc_ids):
        '''performs named entity recognition on each of the article text in db'''
        
        for doc_id in doc_ids:
            
            doc = self.db_collection.find_one({'_id':doc_id})
            
            art_keys = [i for i in doc['articles'] if 'ner' not in doc['articles'][i]]
            for k in art_keys:
                text = doc['articles'][k]['text']
                field_loc = 'articles.{}.ner'.format(k)
                
                ner = self.NLP.get_ent_sents(text)
                ner = [ner[i] for i in ner]
                
                self.db_collection.update_one({'_id':doc_id}, { '$set': {field_loc: ner}})
            
            if len(art_keys)>0:
                print('NLP extracted for', self.db_collection.find_one({'_id': doc_id})['common_name'],
                                    self.db_collection.find_one({'_id': doc_id})['scientific_name']) 

        
class NER(object):
    '''methods for named entity recognition'''
    
    def __init__(self, spacy_model):
        if spacy_model == 'en':
            self.parser = spacy.load('en')
        elif spacy_model == 'bert':
            self.parser = spacy.load('en_trf_bertbaseuncased_lg')
            
        self.target_ents = ['MONEY', 'GPE', 'ORG', 'PERSON', 'QUANTITY', 'CARDINAL']

    def get_ents(self, text):
        '''returnds types of entities present and the associated words'''
        
        self.parsed_text = self.parser(text)
    
        ents = {}
        for e in self.parsed_text.ents:
            if e.label_ in self.target_ents:
                if e.label_ in ents:
                    ents[e.label_] = list(set(ents[e.label_])  | set([e.text]))
                else:
                     ents[e.label_] = [e.text]
        return ents
    
    def get_ent_sents(self, text):
        '''returns specific sentences in text with target entities'''
                    
        sents = {}
        cnt = 0
        for s in self.parser(text).sents:
            ents = self.get_ents(s.text)
            
            if len(ents) > 0:
                sents[cnt] = {'sent': s.text, 'ents': ents}
                cnt += 1
                    
        return sents

    