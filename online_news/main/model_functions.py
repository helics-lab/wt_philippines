#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 22 15:42:00 2020

@author: rtwik
"""

"""
Contains methods for all model related functions, compiling, training, predictind
and testing
@author: rtwik
"""

from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout
from keras.callbacks import ModelCheckpoint
from sklearn.metrics import precision_recall_fscore_support
import numpy
import os 


class Model_functions(object):
    def __init__(self):
        pass

    def compile_model(self, n_classes=2, n_features=0, **extras):
        ''' builds a simple 3 layer feedforward model'''
        
        if n_classes == 2:
            n_outputs = n_classes - 1
        else:
            n_outputs = n_classes

        model = Sequential()
        model.add(Dense(1000, input_dim=n_features, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(100, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(10, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(n_outputs, activation='sigmoid'))

        model.compile(optimizer='adam', loss='binary_crossentropy',
                      metrics=['binary_accuracy', 'mse'])

        return model

    def train_model(self, model, train_data_gen, dev_data_gen, epochs=1, 
                    model_filepath='', **extras):
        ''' trains the model with data generator and saves best model based on
            validation accuracy '''
        
        CP = ModelCheckpoint(model_filepath,
                             monitor='val_binary_accuracy',
                             verbose=1, save_best_only=True, mode='auto')

        model.fit_generator(train_data_gen, steps_per_epoch=train_data_gen.n_iter,
                            epochs=epochs,
                            validation_data=dev_data_gen,
                            validation_steps=dev_data_gen.n_iter,
                            callbacks=[CP])

        return model

    def load_saved_model(self, model_filepath='', **extras):
        print('loading model {}'.format(model_filepath))

        model = load_model(model_filepath)
        return model

    def evaluate_model(self, model, test_data_gen, **extras):

        score = model.evaluate_generator(test_data_gen, steps=test_data_gen.n_iter)

        return score

    def get_predictions(self, model, test_data_gen):
        
        predictions = model.predict_generator(test_data_gen,
                                              steps=test_data_gen.n_iter)
        return predictions
    
class Metrics(object):
    # module to get precision, recall and fscore over predictions
    def __init__(self):
        pass

    def get_prec_rec_fscore(self, y_true, y_pred, threshold=0.5):
        '''
        claculates the precision, recall, fscore and support
        of a binary output classifier
        '''
        binary_labels = [0, 1]
        y_pred = numpy.piecewise(y_pred, [y_pred <= threshold,
                                          y_pred > threshold],
                                 binary_labels)

        precision, recall, fscore, support = precision_recall_fscore_support(
                                                y_true, y_pred,
                                                average='binary')

        return precision, recall, fscore, support